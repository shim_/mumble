#!/bin/bash

function write_config {
 echo "$1"'='"$2" >> /data/murmur.conf
}

#value key alt
function write_config_if_else {
 	if [ ! -z "$1" ]; then
		write_config "$2" "$1"
 	else
		if [ ! -z "$3" ]; then write_config "$2" "$3"; fi
 	fi
}

function wait_for_mysql {
	if [ ! -z "$DATABASE" ] && [ "$DATABASE" == "MYSQL" ]; then
		PORT=3306
		if [ ! -z "$DB_PORT" ] ; then PORT="$DB_PORT" ;fi
		while ! nc -q 1 $DB_HOST $PORT < /dev/null; do sleep 3; done
	fi
	sleep 3
}

if [ ! -e /data/murmur.conf ]; then
	echo Starting Initialization
	cp /etc/murmur.conf /data/murmur.conf
	write_config_if_else "$SERVER_PASSWORD" 'serverpassword' ''
	write_config_if_else "$MAX_USERS" 'users' ''
	write_config_if_else "$SERVER_TEXT" 'welcometext' ''
	write_config_if_else "$REGISTER_NAME" 'registerName' ''
	write_config_if_else "$BANDWIDTH" 'bandwidth' ''
	if [ ! -z "$SSL" ] && [ "$SSL" != "false" ]; then
		NAME='murmur'
		if [ ! -z "$SSL_FILENAME" ]; then NAME="$SSL_FILENAME"; fi
		write_config 'sslCert' "/data/ssl/$NAME.pem"
		write_config 'sslKey' "/data/ssl/$NAME.key"
	fi
	if [ ! -z "$DATABASE" ] && [ "$DATABASE" == "MYSQL" ]; then 
		if [ ! -z "$DB_NAME" ] && [ ! -z "$DB_HOST" ]; then
			write_config 'dbdriver' 'QMYSQL'
			write_config 'database' "$DB_NAME"
			write_config 'dbhost' "$DB_HOST"
			write_config_if_else "$DB_PORT" 'dbport' '3306'
			write_config_if_else "$DB_USER" 'dbusername' ''
			write_config_if_else "$DB_PASS" 'dbpassword' ''
			write_config_if_else "$DB_PREFIX" 'dbprefix' ''
		else
			echo "Insufficent Mysql configuration"
			exit 1
		fi
	else
		write_config 'database' '/data/db.sqlite'
	fi
	echo Initilization Completed

	wait_for_mysql
	if [ ! -z "$SUPW" ] ; then /usr/bin/murmurd -fg -ini /data/murmur.conf -supw $SUPW ;fi
 
fi
wait_for_mysql
/usr/bin/murmurd -fg -ini /data/murmur.conf
