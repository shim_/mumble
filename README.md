## How do I get set up? ##

### Most basic setup with SQLite ###
```bash
docker run -p 64738:64738 -p 64738:64738/udp shimun/mumble
```

### Or more advanced with MySQL ###
```bash
docker run -p 64738:64738 -p 64738:64738/udp -e DATABASE=MYSQL -e DB_HOST=127.0.0.1 -e DB_NAME=murmur -e DB_USER=<username> -e DB_PASS=<password> shimun/mumble
```
Optional variables to configure MySQL:

* **DB_PREFIX** defaults to *none*: Prefix for tables generated by murmur

* **DB_PORT** defaults to *3306*: Port of your MySQL server, 3306 is the MySQL default

### Or more secure by using your own, trusted certificate ###
```bash
docker run -p 64738:64738 -p 64738:64738/udp -v ./data/:/data -e SSL=true shimun/mumble
```

This requires that you placed your certificate in *./data/ssl/murmur.pem* and your key in the same directory named as *murmur.key*

Optional variables to configure SSL:

* **SSL_FILENAME** defaults to *murmur*(ex. above): The name of your certificates in /data/ssl

Other variables to configure Mumble:

* **SERVER_PASSWORD** defaults to *none*: Password required to enter the server

* **MAX_USERS** defaults to *unlimited*: Maximum amount of users allowed on the server

* **SERVER_TEXT** defaults to *Welcome to this server*: Greeting message

* **REGISTER_NAME** defaults to *Root*: Name of the top level channel

* **BANDWIDTH** defaults to *72000*: Bandwidth limit for voice data in bit/s

* **SUPW** defaults to *autogenerated*: Password for the superuser account, you can lookup the *autogenerated* one in this containers logs 
